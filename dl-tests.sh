#!/bin/bash

tag=0.4.13

set -e

tmp=$(mktemp -d)

trap cleanup EXIT
cleanup() {
    set +e
    [ -z "$tmp" -o ! -d "$tmp" ] || rm -rf "$tmp"
}

unset CDPATH
pwd=$(pwd)

pushd "$tmp"
git clone https://github.com/hij1nx/EventEmitter2.git
cd EventEmitter2
git archive --prefix="test/" --format=tar tags/${tag}:test/ \
    | bzip2 > "$pwd"/tests-${tag}.tar.bz2
popd

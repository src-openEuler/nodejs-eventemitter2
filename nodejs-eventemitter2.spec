%{?nodejs_find_provides_and_requires}
%global enable_tests 0
Name:                nodejs-eventemitter2
Version:             6.4.5
Release:             1
Summary:             A Node.js event emitter implementation with namespaces, wildcards and TTL
License:             MIT
URL:                 https://github.com/hij1nx/EventEmitter2
Source0:             https://github.com/hij1nx/EventEmitter2/archive/v%{version}.tar.gz
Source10:            dl-tests.sh
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
ExclusiveArch:       %{ix86} x86_64 %{arm} noarch
BuildRequires:       nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       npm(nodeunit)
%endif
%description
A Node.js event emitter implementation with namespaces, wildcards,
time to live (TTL) and browser support.

%prep
%autosetup -n EventEmitter2-%{version}

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/eventemitter2
cp -pr package.json index.js lib/ \
    %{buildroot}%{nodejs_sitelib}/eventemitter2
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
%{nodejs_sitelib}/nodeunit/bin/nodeunit test/simple/
%{nodejs_sitelib}/nodeunit/bin/nodeunit test/wildcardEvents/
%endif

%files
%doc README.md
%{nodejs_sitelib}/eventemitter2

%changelog
* Thu Jun 30 2022 baizhonggui <baizhonggui@h-partners.com> - 6.4.5-1
- update to 6.4.5

* Thu Aug 20 2020 Anan Fu <fuanan3@huawei.com> - 0.4.13-1
- package init
